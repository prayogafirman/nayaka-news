
import 'package:json_annotation/json_annotation.dart';
part 'news.g.dart';

@JsonSerializable(explicitToJson: true)

class News{

  String? author;
  String? title;
  String? description;
  String? url;
  String? urlToImage;
  DateTime? publishedAt;
  String? content;
  News({
    this.title,
    this.author,
    this.content,
    this.description,
    this.publishedAt,
    this.url,
    this.urlToImage,
  });

  factory News.fromJson(Map<String, dynamic> json) {
    return _$NewsFromJson(json);
  }

  Map<String, dynamic> toJson() => _$NewsToJson(this);
}