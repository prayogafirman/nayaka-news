
import 'package:get/get.dart';
import 'package:nayakanews/screens/splashscreen/splashscreen_controller.dart';
import 'package:nayakanews/screens/splashscreen/splashscreen_controller.dart';


class SplashScreenBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SplashscreenController>(() => SplashscreenController());
  }
}
