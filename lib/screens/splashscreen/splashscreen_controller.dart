import 'dart:async';

import 'package:get/get.dart';
import 'package:nayakanews/utils/routes.dart';

class SplashscreenController extends GetxController {

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    nextPage();
  }

  void nextPage() {
    Timer(Duration(seconds: 1), ()async{
      Get.offAndToNamed(MyRoutes.home);
    });
  }
}