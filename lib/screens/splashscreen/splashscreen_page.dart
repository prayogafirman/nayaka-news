
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nayakanews/screens/splashscreen/splashscreen_controller.dart';
class SplashscreenPage extends StatelessWidget {
  const SplashscreenPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<SplashscreenController>(
      builder: (c) {
        return Scaffold(
          body: Container(
            child: Center(
              child: Image.asset("assets/img/logo.png",width: 200,),
            ),
          ),
        );
      },
    );
  }
}
