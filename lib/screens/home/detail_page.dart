import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:heroicons/heroicons.dart';
import 'package:intl/intl.dart';
import 'package:nayakanews/const/color_const.dart';
import 'package:nayakanews/const/style.dart';
import 'package:nayakanews/model/news.dart';
import 'package:nayakanews/screens/home/home_controller.dart';

class DetailPage extends StatelessWidget {
  DetailPage({Key? key,required this.news}) : super(key: key);
  News news;
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
        builder: (c){
          return Scaffold(
            backgroundColor: CC.primaryColor,
            appBar: AppBar(
              elevation: .4,
              backgroundColor: CC.primaryColor,
              actions: [
                IconButton(onPressed: (){
                  c.addToLove(news);
                } , icon: HeroIcon(
                  HeroIcons.heart,
                  style: HeroIconStyle.outline,
                  // Outlined icons are used by default.
                  color: Colors.black,
                  size: 30,
                ),)
              ],
            ),
            body: Padding(
              padding: EdgeInsets.all(10),
              child: ListView(
                physics: ClampingScrollPhysics(),
                children: [
                  Hero(
                    tag: 'title-${news.title}',
                    child: Text(news.title!,style: SC.headline,),
                  ),
                  Row(
                    children: [
                      Text(DateFormat('EEE, d MMMM HH.mm,').format(news.publishedAt ?? DateTime.now()),style: SC.hint,),
                      Expanded(
                        child: Text(news.author ?? "Unkown",maxLines: 1,overflow: TextOverflow.ellipsis,),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Hero(
                    tag: 'image-${news.title}',
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: CachedNetworkImage(
                        fit: BoxFit.cover,
                        imageUrl: news.urlToImage ?? "http://via.placeholder.com/350x150",
                        placeholder: (context, url) =>
                            Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color: CC.grey.withOpacity(.5)
                              ),
                              margin: EdgeInsets.all(10),
                              child: Center(
                                child: CircularProgressIndicator(),
                              ),
                            ),
                        errorWidget: (context, url, error) =>
                            Icon(Icons.error),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(news.content ?? "-")
                ],
              ),
            ),
            floatingActionButton: FloatingActionButton(
              onPressed: (){
                c.goToUrl(news.url!);
              },
              tooltip: 'Saved News',
              elevation: 5,
              backgroundColor: CC.primaryColor,
              child: const HeroIcon(
                HeroIcons.globeAlt,
                style: HeroIconStyle.outline,
                // Outlined icons are used by default.
                color: Colors.black,
                size: 30,
              ),
            ),
          );
    });
  }
}
