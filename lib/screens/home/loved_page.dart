import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:heroicons/heroicons.dart';
import 'package:intl/intl.dart';
import 'package:nayakanews/const/color_const.dart';
import 'package:nayakanews/const/style.dart';
import 'package:nayakanews/screens/home/detail_page.dart';
import 'package:nayakanews/screens/home/home_controller.dart';
import 'package:skeletons/skeletons.dart';

class LovedPage extends StatelessWidget {
  const LovedPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (c) {
        return Scaffold(
          backgroundColor: CC.primaryColor,
          appBar: AppBar(
            elevation: .4,
            backgroundColor: CC.primaryColor,
            title: Text("Loved News"),
          ),
          body: !c.isLoading
              ? RefreshIndicator(child: SingleChildScrollView(

            child: Padding(
              padding: EdgeInsets.all(10),
              child: StaggeredGrid.count(
                  crossAxisCount: 4,
                  mainAxisSpacing: 10,
                  crossAxisSpacing: 10,
                  children: c.loved.asMap().entries.map((e) {
                    final index = e.key + 1;
                    final item = e.value;
                    return StaggeredGridTile.count(
                      crossAxisCellCount: index % 5 == 1 ? 4 : 2,
                      mainAxisCellCount: index % 5 == 1 ? 4 : 2,
                      child: InkWell(
                        onTap: (){
                          Get.to(()=>DetailPage(news: item));
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            color: CC.white,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Column(
                            children: [
                              Expanded(
                                  child: Padding(
                                    padding: EdgeInsets.all(5),
                                    child: Hero(
                                      tag: 'image-${item.title}',
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(10),
                                        child: CachedNetworkImage(
                                          fit: BoxFit.cover,
                                          imageUrl: item.urlToImage ?? "http://via.placeholder.com/350x150",
                                          placeholder: (context, url) =>
                                              Container(
                                                decoration: BoxDecoration(
                                                    borderRadius: BorderRadius.circular(5),
                                                    color: CC.grey.withOpacity(.5)
                                                ),
                                                margin: EdgeInsets.all(10),
                                                child: Center(
                                                  child: CircularProgressIndicator(),
                                                ),
                                              ),
                                          errorWidget: (context, url, error) =>
                                              Icon(Icons.error),
                                        ),
                                      ),
                                    ),
                                  )
                              ),
                              Padding(
                                padding: EdgeInsets.all(index % 5 == 1 ? 10 : 6),
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Column(
                                        children: [
                                          Hero(
                                            tag : 'title-${item.title}',
                                            child: Text(
                                              item.title!,
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                              style: SC.title,
                                            ),
                                          ),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          Row(
                                            children: [
                                              if(item.publishedAt != null)
                                                HeroIcon(
                                                  HeroIcons.calendarDays,
                                                  style: HeroIconStyle.outline,
                                                  // Outlined icons are used by default.
                                                  color: CC.grey,
                                                  size: 20,
                                                ),
                                              SizedBox(
                                                width: 10,
                                              ),
                                              if(item.publishedAt != null)
                                                Text(DateFormat('EEE, d MMMM HH.mm').format(item.publishedAt ?? DateTime.now()))
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  }).toList()),
            ),
          ), onRefresh: () async {
            c.getNews();
          })
              : skeletonWidget(),
          // floatingActionButtonLocation: c.fabLocation,
        );
      },
    );
  }

  Widget skeletonWidget() {
    return ListView.builder(
      physics: ClampingScrollPhysics(),
      itemCount: 5,
      itemBuilder: (context, index) => Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          padding: const EdgeInsets.all(8.0),
          decoration: BoxDecoration(color: Colors.white),
          child: SkeletonItem(
              child: Column(
                children: [
                  SizedBox(height: 12),
                  SkeletonAvatar(
                    style: SkeletonAvatarStyle(
                      width: double.infinity,
                      height: 200,
                    ),
                  ),
                  SizedBox(height: 12),
                  Row(
                    children: [
                      Expanded(
                        child: SkeletonParagraph(
                          style: SkeletonParagraphStyle(
                              lines: 3,
                              spacing: 6,
                              lineStyle: SkeletonLineStyle(
                                randomLength: true,
                                height: 10,
                                borderRadius: BorderRadius.circular(8),
                                minLength: MediaQuery.of(context).size.width / 2,
                              )),
                        ),
                      ),
                      SkeletonAvatar(
                        style: SkeletonAvatarStyle(
                            borderRadius: BorderRadius.circular(100)),
                      )
                    ],
                  ),
                ],
              )),
        ),
      ),
    );
  }
}
