import 'dart:async';
import 'dart:convert';
import 'dart:ui';

import 'package:dio/dio.dart' as d;
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:nayakanews/model/news.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class HomeController extends GetxController {

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    controller = ScrollController();
    controller.addListener(listen);
    getNews();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    controller.removeListener(listen);
    controller.dispose();
  }


  late ScrollController controller;
  bool showFab = true;
  bool isElevated = true;
  bool isVisible = true;
  bool isLoading = false;
  List<News> news = [];

  FloatingActionButtonLocation get fabLocation => isVisible
      ? FloatingActionButtonLocation.endContained
      : FloatingActionButtonLocation.endFloat;

  void listen() {
    final ScrollDirection direction = controller.position.userScrollDirection;
    if (direction == ScrollDirection.forward) {
      show();
    } else if (direction == ScrollDirection.reverse) {
      hide();
    }
  }

  void show() {
    if (!isVisible) {
      isVisible = true;
      update();
    }
  }

  void hide() {
    if (isVisible) {
      isVisible = false;
      update();
    }
  }

  void addNewItem() {
  }

  void getNews() async {
    try {
      final SharedPreferences pref = await prefs;
      // pref.clear();
      news = [];
      loved = [];
      isLoading = true;
      update();
      var dio = d.Dio();
      d.Response response = await dio.get('https://newsapi.org/v2/top-headlines?country=us&apiKey=bf0aaafedd0644ca8140d8537d155d53');
      if (response.statusCode == 200) {
        // Request successful, you can access the response data
        var data = response.data;
        for(var e in data['articles']){
          news.add(News.fromJson(e as Map<String, dynamic>));
        }

        List<String> lovedList = pref.getStringList('loved') ?? [];
        // print(lovedList);
        for(var e in lovedList){
          loved.add(News.fromJson(jsonDecode(e) as Map<String, dynamic>));
        }
        print(loved);
      } else {
        // Request failed, handle the error
        print('Request failed with status: ${response.statusCode}');
      }
    } catch (e) {
      // An error occurred during the request
      print('Error: $e');
    } finally {
      isLoading = false;
      update();
    }
  }

  Future<void> goToUrl(String url) async {
    if (!await launchUrl(Uri.parse(url))) {
      throw Exception('Could not launch $url');
    }
  }

  final Future<SharedPreferences> prefs = SharedPreferences.getInstance();

  List<News> loved = [];
  void addToLove(News item) async{
    final SharedPreferences pref = await prefs;
    List<String> lovedList = pref.getStringList('loved') ?? [];
    lovedList.add(jsonEncode(item).toString());
    pref.setStringList('loved', lovedList);
    for(var e in lovedList){
      loved.add(News.fromJson(jsonDecode(e) as Map<String, dynamic>));
    }
    Get.snackbar("Success", "News successfully added to Loved List");

    update();
  }

}