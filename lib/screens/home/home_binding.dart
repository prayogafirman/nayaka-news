
import 'package:get/get.dart';
import 'package:nayakanews/screens/home/home_controller.dart';
import 'package:nayakanews/screens/home/home_controller.dart';
import 'package:nayakanews/screens/splashscreen/splashscreen_controller.dart';
import 'package:nayakanews/screens/splashscreen/splashscreen_controller.dart';


class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeController>(() => HomeController());
  }
}
