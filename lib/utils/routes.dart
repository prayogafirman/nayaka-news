
import 'package:get/get.dart';
import 'package:nayakanews/screens/home/home_binding.dart';
import 'package:nayakanews/screens/home/home_page.dart';
import 'package:nayakanews/screens/splashscreen/splashscreen_binding.dart';
import 'package:nayakanews/screens/splashscreen/splashscreen_page.dart';

class MyRoutes {
  static const String splashscreen = "/";
  static const String home = "/home";
  static const Duration _duration = Duration(milliseconds: 400);

  static String getHome() => home;
  static String getSplashscreen() => splashscreen;

  static List<GetPage> routes = [
    GetPage(
      name: splashscreen,
      page: () => SplashscreenPage(),
      transition: Transition.fade,
      transitionDuration: _duration,
      binding: SplashScreenBinding(),
    ),
    GetPage(
      name: home,
      page: () => HomePage(),
      transition: Transition.fade,
      transitionDuration: _duration,
      binding: HomeBinding()
    ),
    // GetPage(
    //   name: _allTasks,
    //   page: () => const AllTasksPage(),
    //   transition: Transition.zoom,
    //   transitionDuration: _duration,
    // ),
    // GetPage(
    //   name: _createTask,
    //   page: () => CreateTaskPage(),
    //   transition: Transition.zoom,
    //   transitionDuration: _duration,
    // ),
    // GetPage(
    //   name: _taskDetail,
    //   page: () => TaskDetail(),
    //   transition: Transition.zoom,
    //   transitionDuration: _duration,
    // ),
    // GetPage(
    //   name: _tasksByStatus,
    //   page: () => const TasksByStatus(),
    //   transition: Transition.zoom,
    //   transitionDuration: _duration,
    // ),
    // GetPage(
    //   name: _settingsPage,
    //   page: () => const SettingsPage(),
    //   transition: Transition.zoom,
    //   transitionDuration: _duration,
    // )
  ];
}
