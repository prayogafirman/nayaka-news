import 'dart:ui';

class CC{
  static const Color primaryColor = Color(0xFFF1EFE7);
  static const Color secondaryColor = Color(0xFF638AEE);
  static const Color lightGrey = Color(0xFFE5E5E5);
  static final lightPurple = Color(0xFFCE89F1);
  static const Color purple = Color(0xFF9793f0);
  static const Color accentColor = Color(0xFFFF8181);
  static const Color white = Color(0xFFFCFCFF);
  static const Color whiteBg = Color(0xFFEFEFEF);
  static const Color ghostwhite = Color(0xFFF1F1FA);
  static const Color grey = Color(0xFF91919F);
  static const Color black = Color(0xff444444);
  static const Color green = Color(0xFF7DC579);
  static const Color lightGreen = Color(0xFFDEFFDB);
  static const Color lightBlue = Color(0xFFC3D3FB);

  static const Color info = Color(0xFFb6effb);
  static const Color primary = Color(0xFFb6d4fe);
  static const Color danger = Color(0xffdbbaba);
  static const Color success = Color(0xFFbadbcc);

  static const Color buttonColor = Color(0xFFCA3F3A);
  static const Color buttonTextColor = Color(0xFFFFFFFF);

  static const Color textColor = Color(0xFF454D66);
  static const Color placeholderTextColor = Color(0xFFADADAD);

  static const Color disabled1Color = Color(0xFFDADADA);
  static const Color disabled2Color = Color(0xFFADADAD);
  static const Color disabled3Color = Color(0xFF7C7A8E);
}
