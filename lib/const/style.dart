import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:nayakanews/const/color_const.dart';

class SC{
  static TextStyle title = GoogleFonts.nunito(
      fontWeight: FontWeight.bold,
      fontSize: 16,
      color: CC.black
  );
  static TextStyle headline = GoogleFonts.nunito(
      fontWeight: FontWeight.bold,
      fontSize: 20,
      color: CC.black
  );

  static TextStyle hint = GoogleFonts.nunito(
      color: CC.grey
  );
}